import {Body, Controller, Get, HttpCode, HttpStatus, InternalServerErrorException, Post, Query} from "@nestjs/common";
import {UsuarioIdxService} from "./usuario-idx.service";
import {UsuarioConIndicesEntity} from "./usuario-con-indices.entity";

@Controller('usuario-idx')
export class UsuarioIdxController {
    constructor(
        private readonly _usuarioService: UsuarioIdxService) {
    }

    @Get('listar-usuarios-con-idx')
    @HttpCode(HttpStatus.OK)
    async getUsuariosConIdx() {
        try {
            const respuestaUsuario = await this._usuarioService
                .listarUsuariosConIndices();
            return respuestaUsuario;
        } catch (error) {
            throw new InternalServerErrorException(error);
        }
    }

    @Get('listar-usuarios-habilitados-con-idx')
    @HttpCode(HttpStatus.OK)
    async getUsuariosHabilitadosConIdx() {
        try {
            const respuestaUsuario = await this._usuarioService
                .listarUsuariosActivosConIndices();
            return respuestaUsuario;
        } catch (error) {
            throw new InternalServerErrorException(error);
        }
    }

    @Get('listar-usuarios-nombre-con-idx')
    @HttpCode(HttpStatus.OK)
    async getUsuariosNombreConIdx(
        @Query('nombre') nombreUsuario: string
    ) {
        try {
            const respuestaUsuario = await this._usuarioService
                .listarUsuariosConNombreIgualAConIndices(nombreUsuario);
            return respuestaUsuario;
        } catch (error) {
            throw new InternalServerErrorException(error);
        }
    }

    @Post('crear-usuario-idx')
    @HttpCode(HttpStatus.OK)
    async postCrearUsuarioIdx(
        @Body() usuario: UsuarioConIndicesEntity
    ) {
        try {
            const respuestaUsuario = await this._usuarioService
                .crear(usuario);
            return respuestaUsuario;
        } catch (error) {
            throw new InternalServerErrorException(error);
        }
    }

}

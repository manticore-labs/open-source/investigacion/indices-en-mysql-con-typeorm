import {Body, Controller, Get, HttpCode, HttpStatus, InternalServerErrorException, Post, Query} from "@nestjs/common";
import {UsuarioService} from "./usuario.service";
import {UsuarioEntity} from "./usuario.entity";

@Controller('usuario')
export class UsuarioController {
    constructor(
        private readonly _usuarioService: UsuarioService) {
    }

    @Get('listar-usuarios')
    @HttpCode(HttpStatus.OK)
    async getUsuarios() {
        try {
            const respuestaUsuario =
                await this._usuarioService.listarUsuarios();
            return respuestaUsuario;
        } catch (error) {
            throw new InternalServerErrorException(error);
        }
    }

    @Get('listar-usuarios-habilitados')
    @HttpCode(HttpStatus.OK)
    async getUsuariosHabilitados() {
        try {
            const respuestaUsuario = await this._usuarioService
                .listarUsuariosActivos();
            return respuestaUsuario;
        } catch (error) {
            throw new InternalServerErrorException(error);
        }
    }

    @Get('listar-usuarios-nombre')
    @HttpCode(HttpStatus.OK)
    async getUsuariosNombre(
        @Query('nombre') nombreUsuario: string
    ) {
        try {
            const respuestaUsuario = await this._usuarioService
                .listarUsuariosConNombreIgualA(nombreUsuario);
            return respuestaUsuario;
        } catch (error) {
            throw new InternalServerErrorException(error);
        }
    }

    @Post('crear-usuario')
    @HttpCode(HttpStatus.OK)
    async postCrearUsuario(
        @Body() usuario: UsuarioEntity
    ) {
        try {
            const respuestaUsuario = await this._usuarioService
                .crear(usuario);
            return respuestaUsuario;
        } catch (error) {
            throw new InternalServerErrorException(error);
        }
    }

}

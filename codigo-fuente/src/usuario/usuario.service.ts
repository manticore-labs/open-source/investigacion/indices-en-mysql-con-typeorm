import {HttpStatus, Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {Like, Repository} from 'typeorm';
import {UsuarioEntity} from "./usuario.entity";

@Injectable()
export class UsuarioService {
    constructor(
        @InjectRepository(UsuarioEntity)
        private usuarioRepository: Repository<UsuarioEntity>,
    ) {
    }

    async crear(usuario: UsuarioEntity):
        Promise<UsuarioEntity> {
        try {
            const usuarioCreado = await this.usuarioRepository.save(usuario);
            return usuarioCreado;
        } catch (error) {
            throw {
                mensaje: 'No se pudo crear el usuario',
                codigo: HttpStatus.INTERNAL_SERVER_ERROR,
                error
            }
        }
    }

    async listarUsuarios():
        Promise<UsuarioEntity[]> {
        try {
            const usuariosActivos =
                await this.usuarioRepository.find();
            return usuariosActivos;
        } catch (error) {
            throw {
                mensaje: 'No se pudo listar usuarios',
                codigo: HttpStatus.INTERNAL_SERVER_ERROR,
                error
            }
        }
    }

    async listarUsuariosActivos():
        Promise<UsuarioEntity[]> {
        try {
            const usuariosActivos =
                await this.usuarioRepository.find({
                    where: {
                        estaActivo: true
                    }
                });
            return usuariosActivos;
        } catch (error) {
            throw {
                mensaje: 'No se pudo listar usuarios activos',
                codigo: HttpStatus.INTERNAL_SERVER_ERROR,
                error
            }
        }
    }

    async listarUsuariosConNombreIgualA(nombreABuscar: string):
        Promise<UsuarioEntity[]> {
        try {
            const usuariosConNombreIgualA =
                await this.usuarioRepository.find({
                    where: {
                        nombre: Like(nombreABuscar),
                    }
                })
            return usuariosConNombreIgualA;
        } catch (error) {
            throw {
                mensaje:
                    'No se pudo listar los usuarios con nombre igual a' + nombreABuscar,
                codigo: HttpStatus.INTERNAL_SERVER_ERROR,
                error
            }
        }
    }
}

## Índices en MySQL con TypeORM
##### Índice
* [Introducción](#introduccion)
* [Índices en MySQL](#indices-en-mysql)
* [¿Por qué y cuándo usar índices?](#por-que-y-cuando-usar-indices)
* [Índices en TypeORM](#indices-en-typeorm)
* [Herramientas Utilizadas](#herramientas-a-utilizar)
* [Ejemplo Práctico](#ejemplo-practico)
* [Conclusiones](#resultados)
* [Enlaces de ayuda](#enlaces-de-ayuda)
* [Bibliografia](#bibliografia)
### Introduccion
### Indices en MySQL
### Indices en TypeORM
### ¿Por que y cuando usar indices?
### Herramientas a Utilizar
Los programas/paquetes/frameworks que se van a utilizar son:

|**Nombre**|**Descripción**|**Versión**|
|---|---|---|
JetBrains WebStorm |IDE para JavaScript <br>**Build** #WS-193.6494.34 <br> **Runtime version:** 11.0.5+10-b520.38 amd64 <br> **VM:** OpenJDK 64-Bit Server VM | v2019.3.3
|Ubuntu|Sistema operativo basado en GNU/Linux|18.04.3 LTS (Bionic Beaver)|
|Node.js|JavaScript runtime environment|v10.16.3|
NestJS|Marco de trabajo para construir aplicaciones del lado del servidor Node.js| @nestjs/cli@7.0.0|
|TypeORM|Object Relational Mapper (ORM) disponible para TypeScript|@nestjs/typeorm@7.0.0|
|NPM|Manejador de paquetes por defecto para Node.js|v6.9.0|
|Extensión Postman for Chrome|Plataforma de colaboración para el desarrollo de API|v5.5.4|
|Google Chrome |Navegador| v78.0.3904.108|
|Kitematic|Aplicación para gestionar contenedores Docker en Mac, Linux y Windows.|0.17.7-1|

### Ejemplo Practico
![](./recursos/imagenes/decorador-indice-en-columna.png)
![](./recursos/imagenes/decorador-indice-unique.png)
![](./recursos/imagenes/decorador-indice-multiple.png)

### Resultados
#### A nivel de base de datos
![](./recursos/imagenes/duracion-consultas-habilitado.png)
![](./recursos/imagenes/consulta-64-status-idx.png)
![](./recursos/imagenes/consulta-66-status.png)
![](./recursos/imagenes/duracion-consultas-indice-multiple.png)
![](./recursos/imagenes/consulta-48-where-name.png)
![](./recursos/imagenes/consulta-48-where-name-idx.png)

|**Consulta**|**Tiempo (ms) sin índices**|**Tiempo (ms) con índices**|
|---|---|---|
|Listar todos los usuarios|1683|378|
|Listar usuarios habilitados|852|704|
|Listar usuarios con nombre que contenga la palabra "Conrad"|655|188|

### A nivel de consultas HTTP
**Información:**
* Metodo Utilizado: GET
* Codigo Recibido: 200
* Tiempo de Respuesta: 852 ms
![](./recursos/imagenes/consulta-listar-usuarios-habilitado.png)
![](./recursos/imagenes/consulta-listar-usuarios-habilitados-idx.png)
![](./recursos/imagenes/consulta-listar-usuarios.png)
![](./recursos/imagenes/consulta-listar-usuarios-idx.png)
![](./recursos/imagenes/consulta-listar-usuarios-nombre.png)
![](./recursos/imagenes/consulta-listar-usuarios-nombre-idx.png)

|**Consulta**|**Tiempo (ms) sin índices**|**Tiempo (ms) con índices**|
|---|---|---|
|Listar todos los usuarios|1683|378|
|Listar usuarios habilitados|852|704|
|Listar usuarios con nombre que contenga la palabra "Conrad"|655|188|
### Enlaces de Ayuda
* [Guia de Instalación de NestJS](https://docs.nestjs.com/)
* [Guia de Integración de TypeORM en NestJS](https://docs.nestjs.com/techniques/database)
### Bibliografia
* [Indices en TypeORM](https://github.com/typeorm/typeorm/blob/master/docs/indices.md)
* [Anatomía de un índice SQL - USE THE INDEX, LUKE](https://use-the-index-luke.com/es/sql/i%CC%81ndice-anatomi%CC%81a)
* [Ventajas y desventajas de los índices (Bases de Datos)](http://www.tufuncion.com/indices-mysql)

*Autor: Leslie Díaz Y.*

*Fecha: 14/03/2020*

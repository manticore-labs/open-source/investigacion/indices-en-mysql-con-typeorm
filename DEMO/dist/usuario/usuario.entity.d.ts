export declare class UsuarioEntity {
    id: number;
    cedula: string;
    nombre: string;
    apellido: string;
    edad: number;
    estaActivo: boolean;
}

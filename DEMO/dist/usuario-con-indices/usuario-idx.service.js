"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const usuario_con_indices_entity_1 = require("./usuario-con-indices.entity");
let UsuarioIdxService = class UsuarioIdxService {
    constructor(usuarioConIndicesRepository) {
        this.usuarioConIndicesRepository = usuarioConIndicesRepository;
    }
    async crear(usuario) {
        try {
            const usuarioCreado = await this.usuarioConIndicesRepository.save(usuario);
            return usuarioCreado;
        }
        catch (error) {
            throw {
                mensaje: 'No se pudo crear el usuario',
                codigo: common_1.HttpStatus.INTERNAL_SERVER_ERROR,
                error
            };
        }
    }
    async listarUsuariosActivosConIndices() {
        try {
            const usuariosActivos = await this.usuarioConIndicesRepository.find({
                where: {
                    estaActivo: true
                }
            });
            return usuariosActivos;
        }
        catch (error) {
            throw {
                mensaje: 'No se pudo listar los usuarios activos',
                codigo: common_1.HttpStatus.INTERNAL_SERVER_ERROR,
                error
            };
        }
    }
    async listarUsuariosConIndices() {
        try {
            const usuariosActivos = await this.usuarioConIndicesRepository.find();
            return usuariosActivos;
        }
        catch (error) {
            throw {
                mensaje: 'No se pudo listar los usuarios activos',
                codigo: common_1.HttpStatus.INTERNAL_SERVER_ERROR,
                error
            };
        }
    }
    async listarUsuariosConNombreIgualAConIndices(nombreABuscar) {
        try {
            const usuariosConNombreIgualA = await this.usuarioConIndicesRepository.find({
                where: {
                    nombre: typeorm_2.Like(nombreABuscar),
                }
            });
            return usuariosConNombreIgualA;
        }
        catch (error) {
            throw {
                mensaje: 'No se pudo listar los usuarios con nombre igual a' + nombreABuscar,
                codigo: common_1.HttpStatus.INTERNAL_SERVER_ERROR,
                error
            };
        }
    }
};
UsuarioIdxService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(usuario_con_indices_entity_1.UsuarioConIndicesEntity)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], UsuarioIdxService);
exports.UsuarioIdxService = UsuarioIdxService;
//# sourceMappingURL=usuario-idx.service.js.map
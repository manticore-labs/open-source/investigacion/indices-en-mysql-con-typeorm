"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const usuario_idx_service_1 = require("./usuario-idx.service");
const usuario_con_indices_entity_1 = require("./usuario-con-indices.entity");
let UsuarioIdxController = class UsuarioIdxController {
    constructor(_usuarioService) {
        this._usuarioService = _usuarioService;
    }
    async getUsuariosConIdx() {
        try {
            const respuestaUsuario = await this._usuarioService
                .listarUsuariosConIndices();
            return respuestaUsuario;
        }
        catch (error) {
            throw new common_1.InternalServerErrorException(error);
        }
    }
    async getUsuariosHabilitadosConIdx() {
        try {
            const respuestaUsuario = await this._usuarioService
                .listarUsuariosActivosConIndices();
            return respuestaUsuario;
        }
        catch (error) {
            throw new common_1.InternalServerErrorException(error);
        }
    }
    async getUsuariosNombreConIdx(nombreUsuario) {
        try {
            const respuestaUsuario = await this._usuarioService
                .listarUsuariosConNombreIgualAConIndices(nombreUsuario);
            return respuestaUsuario;
        }
        catch (error) {
            throw new common_1.InternalServerErrorException(error);
        }
    }
    async postCrearUsuarioIdx(usuario) {
        try {
            const respuestaUsuario = await this._usuarioService
                .crear(usuario);
            return respuestaUsuario;
        }
        catch (error) {
            throw new common_1.InternalServerErrorException(error);
        }
    }
};
__decorate([
    common_1.Get('listar-usuarios-con-idx'),
    common_1.HttpCode(common_1.HttpStatus.OK),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], UsuarioIdxController.prototype, "getUsuariosConIdx", null);
__decorate([
    common_1.Get('listar-usuarios-habilitados-con-idx'),
    common_1.HttpCode(common_1.HttpStatus.OK),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], UsuarioIdxController.prototype, "getUsuariosHabilitadosConIdx", null);
__decorate([
    common_1.Get('listar-usuarios-nombre-con-idx'),
    common_1.HttpCode(common_1.HttpStatus.OK),
    __param(0, common_1.Query('nombre')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], UsuarioIdxController.prototype, "getUsuariosNombreConIdx", null);
__decorate([
    common_1.Post('crear-usuario-idx'),
    common_1.HttpCode(common_1.HttpStatus.OK),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [usuario_con_indices_entity_1.UsuarioConIndicesEntity]),
    __metadata("design:returntype", Promise)
], UsuarioIdxController.prototype, "postCrearUsuarioIdx", null);
UsuarioIdxController = __decorate([
    common_1.Controller('usuario-idx'),
    __metadata("design:paramtypes", [usuario_idx_service_1.UsuarioIdxService])
], UsuarioIdxController);
exports.UsuarioIdxController = UsuarioIdxController;
//# sourceMappingURL=usuario-idx.controller.js.map
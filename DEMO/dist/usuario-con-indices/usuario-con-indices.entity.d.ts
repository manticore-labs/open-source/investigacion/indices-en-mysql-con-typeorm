export declare class UsuarioConIndicesEntity {
    id: number;
    cedula: string;
    nombre: string;
    apellido: string;
    edad: number;
    estaActivo: boolean;
}

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const usuario_con_indices_entity_1 = require("./usuario-con-indices.entity");
const usuario_idx_service_1 = require("./usuario-idx.service");
const usuario_idx_controller_1 = require("./usuario-idx.controller");
let UsuarioIdxModule = class UsuarioIdxModule {
};
UsuarioIdxModule = __decorate([
    common_1.Module({
        imports: [
            typeorm_1.TypeOrmModule.forFeature([
                usuario_con_indices_entity_1.UsuarioConIndicesEntity,
            ], 'default')
        ],
        providers: [usuario_idx_service_1.UsuarioIdxService],
        controllers: [usuario_idx_controller_1.UsuarioIdxController],
        exports: [usuario_idx_service_1.UsuarioIdxService]
    })
], UsuarioIdxModule);
exports.UsuarioIdxModule = UsuarioIdxModule;
//# sourceMappingURL=usuario-idx.module.js.map
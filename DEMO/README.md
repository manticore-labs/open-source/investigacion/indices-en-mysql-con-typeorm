## Ejecución:

Al ser un proyecto de node.js es necesario mantener el directorio `node_modules` junto a `dist` para ser ejecutado.

Sin embargo, se puede ejecutar el proyecto directamente desde el codigo fuente utilizando el comando:
```text
$npm run start:prod
```
despues de instalar todos los paquetes necesarios con el comando:
```text
$npm install o $npm i
```

Dentro del directorio `DEMO` se encuentra el archivo `package.json` el cual contiene la referencia de las librerias a ser utilizadas.

* En primer lugar, se ejecuta el comando:
```text
$npm install o $npm i
```
* Una vez instalados todos los paquetes y visualizado el directorio `node_modules` ejecutamos el proyecto con el comando:
```text
$npm run start:prod o $node dist/main
```

Para cambiar las especificaciones del servidor de base de datos nos dirigimos al archivo `configuraciones.ts` ubicado en la ruta `codigo-fuente/src/constantes`
```typescript
export const CONFIGURACIONES = {
  bdd: {
    type: 'mysql',
    host: 'localhost', // Direccion IP del servidor
    port: 32769, // Puerto a utilizar
    username: 'user', // Nombre de usuario para acceder a la motor del servidor
    password: '12345678', // Contraseña
    database: 'indices', // Nombre de la base de datos a utilizar
    synchronize: true, // true: sincronizar los cambios de la base de datos en el código
    dropSchema: false, // true:eliminar la base de datos
  },
  crearDatosTest: true, // true: crear datos de prueba
};
```
Y despues probar en modo desarrollador con el comando:
```text
nmp run start:dev
```
En el código fuente del proyecto.

Se mostrará el mensaje `Datos cargados correctamente`/`Datos no creados` cuando el proyecto este listo para utilizar.

##### Informacion del ambiente de ejecución

* **Version de node utilizada:** v10.16.3
* **Version de librerias utilizadas:**
  * **TypeORM**: @nestjs/typeorm@7.0.0
